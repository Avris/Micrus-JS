<?php
foreach (['', '/..', '/../..', '/../../..', '/../../../..'] as $dir) {
    if (file_exists($autoloader = __DIR__ . $dir . '/vendor/autoload.php')) {
        require_once $autoloader;
        break;
    }
}

$minify = $watch = false;
foreach ($argv as $arg) {
    // Minification doesn't work ATM
    // if ($arg === '--minify' || $arg === '-m') { $minify = true; }
    if ($arg === '--watch' || $arg === '-w') { $watch = true; }
}
$input = __DIR__ . '/src/micrusjs.coffee';
$output = __DIR__ . '/src/micrusjs.js.twig';

function compile($input) {
    $pb = new \Symfony\Component\Process\ProcessBuilder();
    $pb->add('/usr/bin/coffee');
    $pb->add('-cp');
    $pb->add('--no-header');
    $pb->add($input);

    $proc = $pb->getProcess();
    $proc->run();
    echo $proc->getErrorOutput();
    return $proc->getOutput();
}

function minify($code) {
    $in = sys_get_temp_dir() . '/uglify_in';
    file_put_contents($in, $code);

    $pb = new \Symfony\Component\Process\ProcessBuilder();
    $pb->add('/usr/bin/uglifyjs');
    $pb->add($in);

    $proc = $pb->getProcess();
    $proc->run();
    echo $proc->getErrorOutput();

    unlink($in);

    return $proc->getOutput();
}

function run($input, $output, $minify) {
    echo "Compiling...\n";
    $code = compile($input);
    if ($minify) {
        echo "Minifying...\n";
        $code = minify($code);
    }

    $code = strtr($code, ['"`' => '"', '`"' => '"']);

    file_put_contents($output, $code);
    echo "Compiled\n";
}

if (!$watch) {
    run($input, $output, $minify);
    return;
}

echo "Watching...\n";
$lastModified = 0;
while(true) {
    clearstatcache();
    while (filemtime($input) <= $lastModified) { clearstatcache(); usleep(100000); }
    $lastModified = filemtime($input);
    run($input, $output, $minify);
}
