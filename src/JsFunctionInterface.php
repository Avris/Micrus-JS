<?php
namespace Avris\Micrus\MicrusJs;

interface JsFunctionInterface
{
    /** @return string */
    public function getJsFunction();
}
