<?php
namespace Avris\Micrus\MicrusJs;

interface JsVarsInterface
{
    /** @return array */
    public function getJsVars();
}
