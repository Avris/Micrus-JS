<?php
namespace Avris\Micrus\MicrusJs;

use Avris\Micrus\Controller\Http\Response;
use Avris\Micrus\Controller\Routing\RoutingExtension;
use Avris\Micrus\Bootstrap\ContainerInterface;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Tool\Cache\Cacher;
use Avris\Micrus\Localizator\Localizator;
use Avris\Micrus\Localizator\Order\TranslationOrder;
use Avris\Micrus\View\TemplateDirsProvider;

class MicrusJs implements TemplateDirsProvider, RoutingExtension
{
    protected $vars = [];

    /**
     * @param JsVarsInterface[] $varProviders
     */
    public function __construct($varProviders = [])
    {
        foreach ($varProviders as $varProvider) {
            $this->vars = array_merge($this->vars, $varProvider->getJsVars());
        }
    }

    public function getTemplateDirs()
    {
        return __DIR__;
    }

    public function getRouting()
    {
        return [
            'micrusjs' => '/_micrusjs -> @micrusjs/generateJs',
        ];
    }

    public function generateJsAction(ContainerInterface $container)
    {
        /** @var Localizator $localizator */
        $localizator = $container->get('localizator');

        /** @var Cacher $cacher */
        $cacher = $container->get('cacher');

        /** @var RouterInterface $router */
        $router = $container->get('router');

        $order = $localizator->getTranslationOrder();

        return new Response(
            $container->get('templater')->render([
                '_view' => 'micrusjs.js.twig',
                'routing' => $router->getRoutes(),
                'routeBase' => $router->prependToUrl(''),
                'localeSets' => $this->buildLocaleSets($cacher, $localizator, $order),
                'translationOrder' => $order,
                'localizatorDefaultPatterns' => $localizator->getDefaultPatterns(),
                'localizatorSelectors' => $this->buildLocalizatorSelectors($localizator),
                'roles' => $container->get('roleChecker')->getRoles(),
                'vars' => $this->vars,
            ]),
            200,
            ['content-type' => 'application/javascript']
        );
    }

    /**
     * @param Cacher $cacher
     * @param Localizator $localizator
     * @param string[] $order
     * @return array
     */
    protected function buildLocaleSets(Cacher $cacher, Localizator $localizator, array $order)
    {
        return $cacher->cache(
            $this->buildLocaleSetsCacheKey($order),
            function () use ($localizator, $order) {
                $localeSets = [];
                foreach ($localizator->getLocaleSets() as $setName => $localeSet) {
                    $locales = [];
                    foreach ($localeSet->all() as $localeName => $locale) {
                        if (in_array($localeName, $order)) {
                            $locales[$localeName] = $locale->all();
                        }
                    }
                    $localeSets[$setName] = $locales;
                }
                return $localeSets;
            }
        );
    }

    /**
     * @param string[] $order
     * @return string
     */
    protected function buildLocaleSetsCacheKey(array $order)
    {
        sort($order);

        return 'localeSets/' . join('_', $order);
    }

    /**
     * @param Localizator $localizator
     * @return string
     */
    protected function buildLocalizatorSelectors(Localizator $localizator)
    {
        $localizatorSelectors = [];
        foreach ($localizator->getSelectors() as $selector) {
            if ($selector instanceof JsFunctionInterface) {
                $localizatorSelectors[] = sprintf(
                    '%s: %s',
                    $selector->getName(),
                    $selector->getJsFunction()
                );
            }
        }

        return '{' . implode(',', $localizatorSelectors) . '}';
    }
}