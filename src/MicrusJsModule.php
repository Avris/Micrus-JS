<?php
namespace Avris\Micrus\MicrusJs;

use Avris\Micrus\Bootstrap\Module;

class MicrusJsModule implements Module
{
    public function extendConfig($env, $rootDir)
    {
        return [
            'services' => [
                'micrusjs' => [
                    'class' => MicrusJs::class,
                    'params' => ['#jsVars'],
                    'tags' => ['templateDirs', 'routingExtension'],
                ],
            ]
        ];
    }
}
