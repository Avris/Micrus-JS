Array.cast = (el) -> if Array.isArray(el) then el else if el == undefined then [] else [el]
Array.intersection = (a, b) ->
  [a, b] = [b, a] if a.length > b.length
  value for value in a when value in b

ucfirst = (str) ->
  str += ''
  f = str.charAt(0).toUpperCase()
  f + str.substr(1)

class @M
  `{% for name, value in vars %}M.{{name}} = {{ value|json_encode|raw }};{% endfor %}`

  @user: `{{ app.user ? {'name': app.user.__toString(), 'id': app.user.identifier, "roles": app.user.getRoles }|json_encode|raw : 'null' }}`
  allRoles = `{{ roles|json_encode|raw }}`
  @isGranted: (requiredRoles = undefined) ->
    requiredRoles = Array.cast(requiredRoles)
    return true unless requiredRoles.length
    return false unless M.user
    for role in M.user.roles
      actualRoles = if allRoles[role] == undefined then [role] else allRoles[role]
      return true if Array.intersection(actualRoles, requiredRoles).length
    false

  assetBase = "`{{ asset('') }}`"
  @asset: (path) -> assetBase + path

  routing = `{{ routing|json_encode|raw }}`

  routeBase = "`{{ routeBase }}`"
  @routeExists: (name) -> routing[name] != undefined
  @route: (name, params = {}, frontController = null) ->
    route = routing[name]
    throw "Route '#{name}' not found" if route == undefined
    out = route['pattern']
    tags = out.match(/{.*?}/ig)
    tags = if tags then tags.map((x) -> x.substr(1, x.length - 2)) else []
    replacements = {}
    for i, tag of tags
      param = params[tag]
      if param == undefined
        if route['defaults'] == undefined or (defaultTag = route['defaults'][tag]) == undefined
          throw 'Parameter ' + tag + ' is required for route ' + name
        param = if i == tags.length - 1 then '' else defaultTag
      else
        delete params[tag]
      replacements['{' + tag + '}'] = param
    for key, replacement of replacements
      out = out.replace(key, replacement)
    get = []
    for key, param of params
      get.push encodeURIComponent(key) + '=' + encodeURIComponent(param)
    routeBase + (if out == '/' then out else out.replace(/\/+$/, '')) + (if get.length then '?' + get.join('&') else '')

  @locale: "`{{ app.locale }}`"
  localeSets = `{{ localeSets|json_encode|raw }}`
  localizatorDefaultPatterns = `{{ localizatorDefaultPatterns|json_encode|raw }}`
  translationOrder = `{{ translationOrder|json_encode|raw }}`
  localizatorSelectors = `{{ localizatorSelectors|raw }}`
  localizatorSelectors[""] = (replacements, versions) ->
    count = Math.abs(replacements['%count%'])

    doesMatch = (count, requirement) ->
      ranges = requirement.split(',')
      for range, j in ranges
        continue unless ranges.hasOwnProperty j
        [min, max] = range.split('-')
        return true if rangeMatches(count, min, max)
      false

    rangeMatches = (count, min, max) ->
      if max == undefined
        return true if min == ''
        return count == parseInt(min)
      if min != '' and max != ''
        return count >= min and count <= max
      if min != '' and max == ''
        return count >= min
      if min == '' and max != ''
        return count <= min

    for version, i in versions
      continue unless versions.hasOwnProperty i
      [requirement, translation] = if matches = version.match(/^\{([\d,-]+)\}(.*)/) then [matches[1], matches[2].trim()] else [(if i == 0 then '1' else '0,2-'), version.trim()]
      return translation if doesMatch(count, requirement)
    return ''


  @l: (word, replacements = {}, set = undefined, locale = undefined) ->
    return '' if not word
    translated = getWord(word, set, locale) or fallbackToDefaultPatterns(word) or word
    replacements = filterReplacements(replacements)

    translated = translated.replace /\[\[(.+?)\]\]/gi, (match, contents, offset, s) =>
      return @l(replaceMany(contents, replacements), replacements, set, locale)

    if matches = translated.match(/^<([A-Za-z0-9]*)>(.*)$/)
      fun = localizatorSelectors[matches[1]]
      versions = matches[2].split('|').map (str) -> str.trim()
      translated = fun(replacements, versions) unless fun == undefined

    replaceMany(translated, replacements)

  replaceMany = (text, replacements) ->
    for key, replacement of replacements
      continue unless replacements.hasOwnProperty key
      text = text.replace(key, replacement)
    text

  getWord = (word, set = undefined, locale = undefined) ->
    checkedSets = if set == undefined then localeSets else if localeSets[set] == undefined then {} else {setName: localeSets[set]}
    order = translationOrderWithForced(locale)
    for locale, i in order
      continue unless order.hasOwnProperty i
      for j, localeSet of checkedSets
        continue unless checkedSets.hasOwnProperty j
        continue unless localeSet[locale] != undefined
        translated = getWordByLocale(word, localeSet[locale])
        return translated unless translated == null
    null

  translationOrderWithForced = (forcedLocale = undefined) ->
    return translationOrder if forcedLocale == undefined
    order = [forcedLocale]
    order.push(forcedLocale.substr(0, 2)) if forcedLocale.length == 5
    order

  fallbackToDefaultPatterns = (word) ->
    for pattern in localizatorDefaultPatterns
      if m = (new RegExp(pattern)).exec(word)
        return ucfirst(m[1])
    null

  getWordByLocale = (word, arr) ->
    parts = word.split('.')
    for i, part of parts
      continue unless parts.hasOwnProperty i
      subarr = arr[part]
      return null if subarr == undefined
      arr = subarr
    arr

  filterReplacements = (replacements = {}) ->
    newReplacements = {}
    for key, replacement of replacements
      continue unless replacements.hasOwnProperty key
      key = if key.match(/%\w+%/) then key else "%#{key}%"
      newReplacements[key] = replacement;
    newReplacements
